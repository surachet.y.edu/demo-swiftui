//
//  HikingView.swift
//  Test-SwiftUI
//
//  Created by Surachat Yaitammasan on 8/10/2562 BE.
//  Copyright © 2562 Surachat Yaitammasan. All rights reserved.
//

import SwiftUI

struct HikingView: View {
     let hike = Hiking.all()
       
       var body: some View {
           
           NavigationView {
               List(hike) { hike in
                   HikingCell(hike: hike)
               }.navigationBarTitle("Hiking")
           }
       }
}

struct HikingView_Previews: PreviewProvider {
    static var previews: some View {
        HikingView()
    }
}


struct HikingCell: View {
    
    let hike: Hiking
    
    var body: some View {
        
        return NavigationLink(destination: HikeDetail(hike: hike)) {
            HStack {
                Image(hike.imageUrl)
                    .resizable().frame(width: 100, height: 100).cornerRadius(20)
                VStack(alignment: .leading) {
                    Text(hike.name)
                    Text(String(format: "%.2f", hike.miles))
                }
            }
        }
    }
}
