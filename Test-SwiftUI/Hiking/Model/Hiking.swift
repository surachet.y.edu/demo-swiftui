//
//  Hiking.swift
//  Test-SwiftUI
//
//  Created by Surachat Yaitammasan on 7/10/2562 BE.
//  Copyright © 2562 Surachat Yaitammasan. All rights reserved.
//

import Foundation
import SwiftUI

struct Hiking: Identifiable {

    let id = UUID()
    let name: String
    let imageUrl: String
    let miles: Double
}

extension Hiking {
    
    static func all() -> [Hiking] {
        
        return [
            Hiking(name: "Hike1", imageUrl: "sal", miles: 10),
            Hiking(name: "Hike2", imageUrl: "tom", miles: 10),
            Hiking(name: "Hike3", imageUrl: "tam", miles: 2)
        ]
    }
}
