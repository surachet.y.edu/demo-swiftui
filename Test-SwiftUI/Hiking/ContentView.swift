//
//  ContentView.swift
//  Test-SwiftUI
//
//  Created by Surachat Yaitammasan on 8/10/2562 BE.
//  Copyright © 2562 Surachat Yaitammasan. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
