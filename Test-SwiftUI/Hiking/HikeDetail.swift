//
//  HikeDetail.swift
//  Test-SwiftUI
//
//  Created by Surachat Yaitammasan on 8/10/2562 BE.
//  Copyright © 2562 Surachat Yaitammasan. All rights reserved.
//

import SwiftUI

struct HikeDetail: View {
    
    let hike: Hiking
    
    @State var zoomed = false
    
    var body: some View {
        VStack {
            Image(hike.imageUrl).resizable().aspectRatio(contentMode: self.zoomed ? .fill : .fit).onTapGesture {
                withAnimation(.default) {
                    self.zoomed.toggle()
                }
            }
            
            VStack {
                Text("This place is \(hike.name)").font(.title)
                Text("\(hike.miles) miles")
            }.navigationBarTitle("\(hike.name)", displayMode: .inline)
            
        }
        
    }
}

struct HikeDetail_Previews: PreviewProvider {
    static var previews: some View {
        HikeDetail(hike: Hiking.all()[0])
    }
}
