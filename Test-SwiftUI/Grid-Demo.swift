//
//  Grid-Demo.swift
//  Test-SwiftUI
//
//  Created by Surachat Yaitammasan on 8/10/2562 BE.
//  Copyright © 2562 Surachat Yaitammasan. All rights reserved.
//

import SwiftUI

struct Grid_Demo: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Grid_Demo_Previews: PreviewProvider {
    static var previews: some View {
        Grid_Demo()
    }
}
